#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include "quizzclass.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
  void handleButton1();
  void handleButton2();
  void handleButton3();
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    Ui::MainWindow *ui;
    QuizzClass qc;
    QPushButton *m_button;
    QPushButton *m_button2;
    QPushButton *m_button3;
    QLabel *label1;
    QLabel *label2;
    int round;
    int points;
};
#endif // MAINWINDOW_H
