#include "quizzclass.h"
QuizzClass::QuizzClass() {
    question.push_back("What is the capital city of Egypt?");
    question.push_back("What is the largest country in Europa?");
    question.push_back( "Which country has most of islands?");
    answer1.push_back("Cap Town");
    answer1.push_back("Germany");
    answer1.push_back("Indonesia");
    answer2.push_back("Cairo");
    answer2.push_back("Spain");
    answer2.push_back("Malaysia");
    answer3.push_back("Helsinki");
    answer3.push_back("Russia");
    answer3.push_back("Cuba");
    correctAnswer.push_back(2);
    correctAnswer.push_back(3);
    correctAnswer.push_back(1);
}
QString QuizzClass::getName() {
    return this->nickname;
}
int QuizzClass::getPoints(){
    return this-> points;
}
/*void QuizzClass::SetAnswersAndQuestions() {
    //blaa blaa blaa
    question[0] = "What is the capital city of Egypt?";
    question[1] = "What is the largest country in Europa?";
    question[2] = "Which country has most of islands?";
    answer1[0]= "Cap Town";
    answer1[1]= "Germany";
    answer1[2]= "Indonesia";
    answer2[0]= "Cairo";
    answer2[1]= "Spain";
    answer2[2]= "";
    answer3[0]= "Helsinki";
    answer3[1]= "Russia";
    answer3[2]= "";
    correctAnswer[0] =2;
    correctAnswer[1] =3;
    correctAnswer[2] =1;

}*/
QString QuizzClass::getQuestion(int nro){
    QString result = question[nro];
    return result;
}
QString QuizzClass::getAnswer1(int nro) {
    return this->answer1[nro];
}
QString QuizzClass::getAnswer2(int nro) {
    return this->answer2[nro];
}
QString QuizzClass::getAnswer3(int nro) {
    return this->answer3[nro];
}
int QuizzClass::getCorrectAnswer(int nro) {
    return this->correctAnswer[nro];
}
void QuizzClass::setRound(int r) {
    this->round = r;
}

