# Quizz Qt

With Qt built little Quiz game.

## Installation
Take git clone of project. Open it in Qt Creator. Build and run. 

## Usage
Questions can be easily appended and game can be made mor fun!

##Requirements
Qt, g++ compiler.

## Contributors
Ilona Skarp for Noroff and Experis Academy, student project. 

Please make sure to update tests as appropriate.
