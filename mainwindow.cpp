#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "quizzclass.h"
#include <QPushButton>

void MainWindow::handleButton1() {
    if(round < 3) {
        if(this->qc.getCorrectAnswer(round)==1) {
            points += 100;
        }
        round++;
        if(round < 3){
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        } else if (round == 3) {
            QString pointsstr = QString::number(points);
             label2->setText(pointsstr);
        }
    }
}
void MainWindow::handleButton2(){
    if(round < 3) {
        if(this->qc.getCorrectAnswer(round)==2) {
            points += 100;
        }
        round++;
        if(round < 3) {
            m_button->setText(qc.getAnswer1(round));
            m_button2->setText(qc.getAnswer2(round));
            m_button3->setText(qc.getAnswer3(round));
            label1->setText(qc.getQuestion(round));
            QString pointsstr = QString::number(points);
            label2->setText(pointsstr);
        } else if (round == 3) {
            QString pointsstr = QString::number(points);
             label2->setText(pointsstr);
        }

    }
}
    void MainWindow::handleButton3(){
        if(round < 3) {
            if(this->qc.getCorrectAnswer(round)==3) {
                points += 100;
            }
            round++;
            if(round < 3) {
                m_button->setText(qc.getAnswer1(round));
                m_button2->setText(qc.getAnswer2(round));
                m_button3->setText(qc.getAnswer3(round));
                label1->setText(qc.getQuestion(round));
                QString pointsstr = QString::number(points);
                label2->setText(pointsstr);
            } else if (round == 3) {
                QString pointsstr = QString::number(points);
                 label2->setText(pointsstr);
            }
        }
    }

    MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent)
        , ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        this->round = 0;
        this->points = 0;
        QuizzClass qc  = QuizzClass();
        this->m_button = new QPushButton(qc.getAnswer1(this->round), this);
        // set size and location of the button
        this->m_button->setGeometry(QRect(QPoint(100, 100), QSize(200, 50)));
        connect(m_button, SIGNAL(clicked()), this, SLOT(handleButton1()));
        this->m_button2 = new QPushButton(qc.getAnswer2(this->round), this);
        // set size and location of the button
        this->m_button2->setGeometry(QRect(QPoint(300, 100), QSize(200, 50)));
        connect(m_button2, SIGNAL(clicked()), this, SLOT(handleButton2()));
        this->m_button3 = new QPushButton(qc.getAnswer3(this->round), this);
        // set size and location of the button
        this->m_button3->setGeometry(QRect(QPoint(500, 100), QSize(200, 50)));
        connect(m_button3, SIGNAL(clicked()), this, SLOT(handleButton3()));
        this->label1 = new QLabel(this);
        label1->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        label1->setWordWrap(true);
        label1->setText(qc.getQuestion(round));
        label1->setGeometry(QRect(10,50,300,50));
        label1 ->setVisible(true);
        this->label2 = new QLabel(this);
        label2->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        label2->setWordWrap(true);
        QString pointsstr = QString::number(points);
        label2->setText(pointsstr);
        label2->setGeometry(QRect(10,50,140,15));
        label2 ->setVisible(true);



    }

    MainWindow::~MainWindow()
    {
        delete ui;
    }


