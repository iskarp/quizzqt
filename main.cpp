#include "mainwindow.h"

#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setFont (QFont ("Times", 11, QFont::Normal));
    MainWindow w;
    w.show();
    return a.exec();
}
